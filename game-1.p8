pico-8 cartridge // http://www.pico-8.com
version 29
__lua__
local player={}
local coin={}

function _init()
   -- all the code that
   -- happens when the
   -- cart is first run

   player = {
      x=64,
      y=64,
      width=8,
      height=8,
      move_speed=1,
      is_vertically_aligned=false,
      is_horizontally_aligned=false,
      -- move the player
      update=function(self)
         if btn(1) then
            self.x+=self.move_speed -- move right
         end
         if btn(0) then
            self.x-=self.move_speed -- move left
         end
         if btn(3) then
            self.y+=self.move_speed -- move down
         end
         if btn(2) then
            self.y-=self.move_speed -- move up
         end
         -- play sound when button pressed
         if btnp(4) then
            sfx(0)
         end
         -- check for player alignment
         local left=self.x
         local right=self.x+self.width
         if left<coin.x and coin.x<right then
            self.is_vertically_aligned=true
         else
            self.is_vertically_aligned=false
         end
         local top=self.y
         local bottom=self.y+self.height
         if top<coin.y and coin.y<bottom then
            self.is_horizontally_aligned=true
         else
            self.is_horizontally_aligned=false
         end
         -- collect object
         if self.is_horizontally_aligned and self.is_vertically_aligned then
            coin.is_collected=true
         end
      end,
      draw=function(self)
         spr(1,self.x,self.y)
         print(self.is_vertically_aligned,self.x+10,self.y,7)
         print(self.is_horizontally_aligned,self.x+10,self.y+7,6)
      end
   }

   coin = {
      x=80,
      y=100,
      is_collected=false,
      update=function(self)
         -- empty for now
      end,
      draw=function(self)
         if not self.is_collected then
            spr(4,self.x-3,self.y-4)
            pset(self.x,self.y,7)
         end
      end
   }

end

function _update()
	-- all the code that
	-- updates the game
   player:update()
   coin:update()
end

function _draw()
	-- all the code that
	-- draws stuff on screen
	cls(1) -- pass in a color
	player:draw()
  coin:draw()
end
__gfx__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000110000000000000000000009aa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
007007000017c100000110000000800009aaaa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000001cc1000017c1000008980009aaaa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0007700000011000001cc1000088980009aaaa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
007007000222222000011000009a998009aaaa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000002200000222200089aa980009aa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000002002000022220088aaaa98000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
0101000014050180002d5102d5202e5302e5302f5402f5402f5402f5402f5402f5402f530280202805028050260402502021010285102751026510235102052014020140301a0302f7302e7302e7302e7502d750
011200003932037320343203232000020303202f3202e3201e32010320003202532023320203201e3201d3201a3201732015320153200002012320000200f3200c3200b3200a3200832008320073200632000020
